import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AnimalDetailComponent } from './animal-detail.component';

export const AnimalDetailRouting = RouterModule.forChild([
    { path: 'Animal/:id', component: AnimalDetailComponent },
]);

