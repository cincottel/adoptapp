import { Component, OnInit, OnDestroy } from '@angular/core';
import { Breadcrumb } from '../shared/page-title/breadcrumb';
import { Animal } from '../models/animal';
import { Observable } from 'rxjs/Rx';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { AdoptionSearchService } from './adoption-search.service';

@Component({
  selector: 'app-adoptions',
  templateUrl: './adoption-search.component.html',
})
export class AdoptionSearchComponent implements OnInit, OnDestroy {
  public title = 'Adopciones / Buscar';
  public breadcrumbs = [new Breadcrumb('Principal', '/'), new Breadcrumb('Adopciones / Buscar', '', true)];

  public animals;
  public subscription;

  public page = 1;
  public pages;
  public total;

  public isLoading = true;

  form = new FormGroup({
    specieDog: new FormControl(''),
    specieCat: new FormControl(''),
    specieBird: new FormControl(''),
    specieHorse: new FormControl(''),
    sizeL: new FormControl(''),
    sizeM: new FormControl(''),
    sizeS: new FormControl(''),
    sort: new FormControl('')
  });

  constructor(private service: AdoptionSearchService) { }

  ngOnInit() {
    this.subscription = this.service.getFilteredAdoptions(this.setFormData()).subscribe( data => {
      this.animals = data['animals'];
      this.total  = data['total'];
      this.page  = data['page'];

      const numPages = Math.ceil(this.total / 9);
      this.pages = Array(numPages).fill(null).map((x, i) => i + 1);

      this.isLoading = false;
    });

    this.form.valueChanges
      .distinctUntilChanged()
      .subscribe( value => {
        const formData = this.setFormData();
        this.isLoading = true;

        this.service.getFilteredAdoptions(formData).subscribe( data => {
          this.animals = data['animals'];
          this.total  = data['total'];
          this.page  = data['page'];

          const numPages = Math.ceil(this.total / 9);
          this.pages = Array(numPages).fill(null).map((x, i) => i + 1);
          this.isLoading = false;
        });
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  setFormData(page = 1) {
    const formData = { sizes: [], species: [], page: page, sort: this.form.controls.sort.value };

    if (this.form.controls.specieDog.value) {
      formData.species.push('Perro');
    }

    if (this.form.controls.specieCat.value) {
      formData.species.push('Gato');
    }

    if (this.form.controls.specieBird.value) {
      formData.species.push('Ave');
    }

    if (this.form.controls.specieHorse.value) {
      formData.species.push('Caballo');
    }

    if (this.form.controls.sizeL.value) {
      formData.sizes.push('L');
    }

    if (this.form.controls.sizeM.value) {
      formData.sizes.push('M');
    }

    if (this.form.controls.sizeS.value) {
      formData.sizes.push('S');
    }

    return formData;
  }

  loadFromPage(pageNum) {
    const formData = this.setFormData(pageNum);

    this.service.getFilteredAdoptions(formData).subscribe( data => {
      this.animals = data['animals'];
      this.total  = data['total'];
      this.page  = data['page'];

      const numPages = Math.ceil(this.total / 9);
      this.pages = Array(numPages).fill(null).map((x, i) => i + 1);
    });
  }

  changeSort($event) {
    this.form.controls.sort.setValue($event.srcElement.value);
  }

}
