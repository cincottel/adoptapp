import { Injectable } from '@angular/core';
import {Adoption} from './adoption';
import { Animal } from '../models/animal';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GLOBAL } from '../Services/global';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class AdoptionSearchService {

    constructor(private _http: HttpClient) {}

    // Metodo que extrae todos los animales
    getAdoptions() {
        return this._http.get('https://backend-adoptapp.herokuapp.com/api/animals');
    }

    getAdoptionDetail(id: String) {
        return this._http.get<Animal>('https://backend-adoptapp.herokuapp.com/api/animal/' + id);
    }

    getFilteredAdoptions(data) {
        const params = JSON.stringify(data);

        return this._http.post(GLOBAL.url + 'animals/byFilter', params,
            { headers: new HttpHeaders().set('Content-Type', 'application/json' ) });
    }
}
