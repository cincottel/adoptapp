import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdoptionSearchComponent } from './adoption-search.component';
import { AdoptionDetailComponent } from './adoption-detail/adoption-detail.component';

export const AdoptionSearchRouting = RouterModule.forChild([
    { path: 'adoptions/:id', component: AdoptionDetailComponent },
    { path: 'adoptions', component: AdoptionSearchComponent }
]);

