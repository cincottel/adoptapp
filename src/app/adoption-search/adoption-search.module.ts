import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageTitleModule } from '../shared/page-title/page-title.module';
import { AdoptionSearchComponent } from './adoption-search.component';
import { AdoptionDetailComponent } from './adoption-detail/adoption-detail.component';
import { RouterModule } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { AdoptionSearchService } from './adoption-search.service';

import { AdoptionImagePipe } from '../pipes/adoption-image.pipe';

import { NgxGalleryModule } from 'ngx-gallery';

@NgModule({
  declarations: [
    AdoptionSearchComponent, AdoptionDetailComponent, AdoptionImagePipe
  ],
  imports: [
    RouterModule, PageTitleModule, CommonModule, HttpClientModule, NgxGalleryModule, ReactiveFormsModule, FormsModule
  ],
  providers: [AdoptionSearchService]
})
export class AdoptionSearchModule { }
