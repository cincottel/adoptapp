export class Adoption {
    public name: string;
    public age: number;
    public images: Array<string>;
    public description: string;
    public views: number;
    public location: string;
    public size: string;

    constructor(name, age, images, description, views, location, size) {
        this.name = name;
        this.age = age;
        this.images = images;
        this.description = description;
        this.views = views;
        this.location = location;
        this.size = size;
    }
}
