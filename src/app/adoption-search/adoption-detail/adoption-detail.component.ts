import { Component, OnInit, OnDestroy } from '@angular/core';
import { Animal } from '../../models/animal';
import { Breadcrumb } from '../../shared/page-title/breadcrumb';
import { AdoptionSearchService } from '../adoption-search.service';
import { GLOBAL } from '../../Services/global';
import { AnimalService } from '../../Services/animal.services';
import { Router, ActivatedRoute, Params } from '@angular/router';


import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';

import { Route } from '@angular/router/src/config';
import { UserService } from '../../Services/user.service';


@Component({
    selector: 'app-detail',
    templateUrl: 'adoption-detail.component.html',
    providers: [AnimalService, UserService]
})

export class AdoptionDetailComponent implements OnInit, OnDestroy {
    public id: string;
   // public adoption: Animal;
    public subscribe;

    // para la adopcion
    public url: String;
    public animal: Animal;
    public identity;
    public token;
    public status;
    public image;

    galleryOptions = [
        {
            width: '100%',
            height: '600px',
            thumbnailsColumns: 4,
            imageAnimation: NgxGalleryAnimation.Slide,
            previewCloseOnClick: true,
            previewCloseOnEsc: true
        }
    ];
    galleryImages: NgxGalleryImage[];

    public title = 'Detalle';
    public breadcrumbs = [new Breadcrumb('Principal', '/'),
    new Breadcrumb('Adopciones / Buscar', '/adoptions'),
    new Breadcrumb('Detalle', '', true)];

    constructor(
        private _service: AdoptionSearchService,
        private _route: ActivatedRoute,
        private _AnimalService: AnimalService,
        private _router: Router,
        private _userService: UserService
    ) {
        this.url = GLOBAL.url;
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();
    }

    ngOnInit() {
        this.getAnimal();
    }

    ngOnDestroy() {}

    getAnimal() {
        this._route.params.forEach((params: Params) => {
            const id = params['id'];

            this._AnimalService.getAnimal(id).subscribe(
                response => {
                    if (!response.animal) {
                        this._router.navigate(['/']);
                    } else {
                        this.animal = response.animal;
                        this.image  = this.animal.Image;
                    }

                },
                error => {
                    console.log(<any>error);
                    this._router.navigate(['/']);
                }
            );

        });
    }

    AdoptAnimal(id) {
        this._AnimalService.adoptAnimal(this.token, id, this.animal).subscribe(
            response => {
                if (!response.animal) {
                    this.status = 'Error';
                } else {
                    this.status = 'Success';
                    this.animal.UserAdopted = this.identity._id;
                    this.animal.Adopted = true;
                    this._router.navigate(['/myadoptions']);
                }
            },
            error => {
                const errorMessage = <any>error;

                if (errorMessage != null) {
                    this.status = 'Error';
                }

            }
        );
    }

}
