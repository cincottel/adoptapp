import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import { NotFoundComponent } from './shared/not-found/not-found.component';
import { HomeComponent } from './home/home.component';

export const routing = RouterModule.forRoot([
    {path: '', component: HomeComponent},
    {path: '**', component: NotFoundComponent}
]);
