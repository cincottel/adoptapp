import { NgModule } from '@angular/core';

import { PageTitleModule } from '../shared/page-title/page-title.module';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    RouterModule, PageTitleModule
  ],
  exports: [
    HomeComponent
  ],
  providers: []
})
export class HomeModule { }
