import { Component } from '@angular/core';
import { Breadcrumb } from '../shared/page-title/breadcrumb';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
  public title = 'Principal / Dashboard';
  public breadcrumbs = [new Breadcrumb('Principal', '#'), new Breadcrumb('Principal', '#')];
}


