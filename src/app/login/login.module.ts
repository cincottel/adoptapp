import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageTitleModule } from '../shared/page-title/page-title.module';

import { LoginFormComponent } from './login-form/login-form.component';
import { LoginComponent } from './login.component';
import { RegisterComponent } from './register/register.component';

import { FacebookModule } from 'ngx-facebook';

import { RouterModule } from '@angular/router';

import {FormsModule} from '@angular/forms';

import { FBInfoService } from './fb-info.service';


import { HttpClientModule, HttpClient } from '@angular/common/http';

@NgModule({
  declarations: [
    LoginComponent, LoginFormComponent, RegisterComponent
  ],
  imports: [
    RouterModule, CommonModule, PageTitleModule, FacebookModule.forRoot(), FormsModule, HttpClientModule,
  ],
  providers: [FBInfoService, HttpClient]
})
export class LoginModule { }
