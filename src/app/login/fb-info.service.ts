import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class FBInfoService {
    constructor(private _http: HttpClient) {}

    getFacebookInfo(userId, accessToken) {
        return this._http.get('https://graph.facebook.com/' +
        userId + '?fields=email,age_range,picture,id,first_name,gender,last_name&access_token=' +
        accessToken);
    }
}
