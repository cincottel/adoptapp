import { Component, OnInit } from '@angular/core';
import { Breadcrumb } from '../shared/page-title/breadcrumb';
import { PageTitleComponent } from '../shared/page-title/page-title.component';

import { RegisterComponent } from './register/register.component';
import { LoginFormComponent } from './login-form/login-form.component';

import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    public title = 'Inicio de sesión / Registro';
    public breadcrumbs = [new Breadcrumb('Principal', '#'), new Breadcrumb(this.title, '#')];

    constructor(private _router: Router) { }

    ngOnInit() {
        // esto esta mal xq lo q dice es q antes q cargue el login component haga un routing a home lo cual nada q ver
        // if (identity) {
        //    this._router.navigate(['/']);
        // }
    }

}

