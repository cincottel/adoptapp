import { Component, OnInit } from '@angular/core';
import { FacebookService, LoginResponse, InitParams } from 'ngx-facebook';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../../Services/user.service';
import { Observable } from 'rxjs/Observable';
import { FBInfoService } from '../fb-info.service';

@Component({
    selector: 'app-login-form',
    templateUrl: 'login-form.component.html',
    providers: [UserService]
})

export class LoginFormComponent implements OnInit {
    public title: String;
    public user: User;
    public identity;
    public token;
    public status: String;
    public subscribe;

    constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService,
        private _fb: FacebookService, public _fbInfo: FBInfoService) {

        this.title = 'Login';
    }

    // On init method
    ngOnInit() {
        const initParams: InitParams = {
            appId: '1435267633238782',
            xfbml: true,
            version: 'v2.11'
        };


        this._fb.init(initParams);
        // console.log(this._userService.getIdentity());
        // console.log(this._userService.getToken());

        this.user = new User('', '', '', '', '', 'ROLE_USER', '');
    }

    loginWithFacebook() {
        let fbToken;
        let fbTokenExpires;
        let fbUserId;

        const component = this;

        if (this.identity) {
            this._router.navigate(['/']);
        }

        this._fb.login()
            .then(function (response: LoginResponse) {
                if (response && response.status === 'connected') {
                    component.user = new User('', '', '', '', '', 'ROLE_USER', '');

                    fbToken = response.authResponse.accessToken;
                    fbTokenExpires = response.authResponse.expiresIn;
                    fbUserId = response.authResponse.userID;
                    console.log(response);
                    localStorage.setItem('fbToken', fbToken);

                    component._fbInfo.getFacebookInfo(fbUserId, fbToken).subscribe((data) => {
                        component.user.Name = data['first_name'];
                        component.user.LastName = data['last_name'];
                        component.user.fbFirstName = data['first_name'];
                        component.user.fbLastName = data['last_name'];
                        component.user.fbAgeRange = data['age_range']['min'];
                        component.user.fbUserId = fbUserId;
                        component.user.fbPicture = data['picture']['data']['url'];
                        component.user.fbUser = true;
                        component.user.fbGender = data['gender'];
                        component.user.Email = data['email'];

                        component._userService.signup(component.user).subscribe(userResponse => {
                            component.identity = userResponse.user;
                            if (!component.identity || !component.identity._id) {
                                alert('El usuario no inicio session correctamente');
                            } else {
                                component.identity.Password = '';
                                localStorage.setItem('identity', JSON.stringify(component.identity));
                                component._userService.signup(component.user, true).subscribe(
                                    tokenResponse => {
                                        component.token = tokenResponse.token;
                                        if (component.token.lenght <= 0) {
                                            alert('El token no se ha generado');
                                        } else {
                                            localStorage.setItem('token', component.token);
                                            component.status = 'Success';
                                            component._router.navigate(['/']);
                                        }
                                    },
                                    error => {
                                        console.log(<any>error);
                                    }
                                );
                            }
                        }, error => {
                            const errorMessage = <any>error;
                            if (errorMessage != null) {
                                const body = JSON.parse(error._body);
                                console.log(body);
                            }
                        });

                    });
                }
            })
            .catch(function (error: any) {
                console.error(error);
            });
    }

    onSubmit() {
        this._userService.signup(this.user).subscribe(

            response => {
                this.identity = response.user;

                if (!this.identity || !this.identity._id) {
                    alert('El usuario no inicio session correctamente');
                } else {
                    // Mostrar identity
                    this.identity.Password = '';
                    // console.log(this.identity);
                    localStorage.setItem('identity', JSON.stringify(this.identity));
                    // conseguir el token
                    this._userService.signup(this.user, true).subscribe(

                        Response => {
                            this.token = Response.token;

                            if (this.token.lenght <= 0) {
                                alert('El token no se ha generado');
                            } else {
                                // mostrar token
                                // console.log(this.token);
                                localStorage.setItem('token', this.token);
                                this.status = 'Success';
                                this._router.navigate(['/']);
                            }
                        },
                        error => {
                            console.log(<any>error);
                        }
                    );
                }
            },
            error => {
                const errorMessage = <any>error;

                if (errorMessage != null) {
                    const body = JSON.parse(error._body);
                    this.status = 'Error';
                }
            }

        );

    }
}

