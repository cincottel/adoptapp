
import { Pipe, PipeTransform } from '@angular/core';
import { GLOBAL } from '../Services/global';

@Pipe({name: 'adoptionimg'})

export class AdoptionImagePipe implements PipeTransform {
    transform(value: string) {
        if (value) {
            return GLOBAL.url + 'get-Image-Animal/' + value;
        }
        return '../assets/vendor/images/doge.jpg';
    }
}
