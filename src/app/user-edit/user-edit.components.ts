import { Component, OnInit, DoCheck } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../models/user';
import { GLOBAL } from '../Services/global';
import { UserService } from '../Services/user.service';
import { UploadService } from '../Services/upload.service';
import { Breadcrumb } from '../shared/page-title/breadcrumb';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  providers: [
    UserService,
    UploadService
  ]
})
export class UserEditComponent implements OnInit, DoCheck {

  public title: String;
  public user: User;
  public identity;
  public token;
  public status: string;
  public url: string;


  public breadcrumbs = [
    new Breadcrumb('Principal', '/'),
    new Breadcrumb('Mi perfil', '', true)
];

  public filesToUpload: Array<File>;
  fileChangeEvent(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    //  console.log(this.filesToUpload);
  }

  constructor(private _userService: UserService, private _uploadService: UploadService, private _router: Router) {

    this.title = 'Mi perfil';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.user = this.identity;
    this.url = GLOBAL.url;

  }

  ngOnInit() {
    // console.log('Edit-User cargado');
    this.identity = this._userService.getIdentity();
  }

  ngDoCheck() {
    this.identity = this._userService.getIdentity();
  }

  onSubmit() {
    this._userService.updateUser(this.user).subscribe(
      response => {
        if (!response.user) {
          this.status = 'Error';
        } else {
          this.status = 'Success';
          localStorage.setItem('identity', JSON.stringify(this.user));

          // subida de imagen
          this._uploadService.makeFileRequest(this.url + 'UploadUserImage/' + this.user._id, [], this.filesToUpload, this.token, 'image')
            .then((result: any) => {
              this.user.Image = result.Image;
              localStorage.setItem('identity', JSON.stringify(this.user));
              this.identity = this._userService.getIdentity();
              this._router.navigate(['/']);
              //  console.log(this.user);
            });

        }

      },
      error => {
        const errorMessage = <any>error;

        if (errorMessage != null) {
          this.status = 'Error';
        }
      }

    );
  }
}
