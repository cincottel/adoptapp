import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserEditComponent } from './user-edit.components';

export const UserEditRouting = RouterModule.forChild([
    { path: 'myprofile', component: UserEditComponent },
]);

