import { Component, OnInit, DoCheck } from '@angular/core';

import { UserService } from './Services/user.service';
import { Router } from '@angular/router';
import { GLOBAL } from './Services/global';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [UserService]
})
export class AppComponent implements OnInit, DoCheck {
  public alerts = [];
  public title = 'AdoptApp';
  public url: String;
  public identity;
  public Colors: string[] = ['#001f3f', '#E9E581', '#111111'];


  constructor(private _userService: UserService, private _router: Router) {
    this.url = GLOBAL.url;
  }

  ngDoCheck() {
    this.identity = this._userService.getIdentity();
  }

  ngOnInit() {
    this.identity = this._userService.getIdentity();
  }

  Logout() {
    localStorage.clear();
    this.identity = null;
    this._router.navigate(['/']);
  }

  CambiaColor(ColorSelected) {
    $(document).ready(function () {
      $('nav').css('background-color', ColorSelected);
    });


  }
}
