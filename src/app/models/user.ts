export class User {
    constructor(
        public _id: string,
        public Name: string,
        public LastName: string,
        public Email: string,
        public Password: string,
        public Role: string,
        public Image: string,
        
        // Facebook info
        public fbUser: boolean = false,
        public fbUserId: string = '',
        public fbAgeRange: string = '',
        public fbPicture: string =  '',
        public fbFirstName: string = '',
        public fbLastName: string = '',
        public fbGender: string = '',

    ) { }
}
