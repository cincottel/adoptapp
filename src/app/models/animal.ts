export class Animal {
    constructor(
        public _id: string,
        public Name: string,
        public Description: string,
        public Year: number,
        public Image: string,
        public Adopted: boolean,
        public User: string,
        public Size: string,
        public Especie: string,
        public Raza: string,
        public UserAdopted: string
    ) { }
}
