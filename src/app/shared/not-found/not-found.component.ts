import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-not-found',
    template: `
        <h1>No se encontró la página</h1>
    `
})

export class NotFoundComponent {}
