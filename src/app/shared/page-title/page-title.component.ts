import { Breadcrumb } from './breadcrumb';
import { Component, Input } from '@angular/core';


@Component({
    selector: 'app-title',
    templateUrl: './page-title.component.html'
})

export class PageTitleComponent {
    @Input() title: string;
    @Input() breadcrumbs: Array<Breadcrumb>;

    constructor() {}
}
