export class Breadcrumb {
    label: string;
    url: string;
    current: boolean;

    constructor(label, url, current = false) {
        this.label = label;
        this.url = url;
        this.current = current;
    }
}

