import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../models/user';
import { GLOBAL } from '../Services/global';
import { UserService } from '../Services/user.service';
import { AnimalService } from '../Services/animal.services';
import { Animal } from '../models/animal';
import { Breadcrumb } from '../shared/page-title/breadcrumb';

@Component({
    selector: 'app-mis-adopciones',
    templateUrl: './mis-adopciones.component.html',
    providers: [
        UserService,
        AnimalService
    ]
})
export class MisAdopcionesComponent implements OnInit {

    public title: String;
    public url: String;
    public animal: Animal;
    public animals: Animal[];
    public identity;
    public token;
    public status;
    public busqueda;

    public breadcrumbs = [
        new Breadcrumb('Principal', '/'),
        new Breadcrumb('Mis adopciones', '', true)
    ];

    constructor(
        private _animalService: AnimalService,
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService,

    ) {
        this.title = 'Mis mascotas adoptadas';
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();

    }

    ngOnInit() {
        this.getAnimalsByUserId();
    }
    getAnimalsByUserId() {
        this._animalService.getAnimalAdoptedByUser(this.token, this.identity._id).subscribe(
            response => {
                if (!response.animals) {

                } else {
                    this.animals = response.animals;
                }
            },
            error => {
                console.log(<any>error);
            }
        );
    }

}

