import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MisAdopcionesComponent } from './mis-adopciones.component';

export const MisAdopcionesRouting = RouterModule.forChild([
    { path: 'myadoptions', component: MisAdopcionesComponent },
]);

