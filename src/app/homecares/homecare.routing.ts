import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeCareComponent } from './homecare.component';

export const HomeCareRouting = RouterModule.forChild([
    { path: 'homecares', component: HomeCareComponent },
]);

