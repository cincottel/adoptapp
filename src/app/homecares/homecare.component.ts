import { Component, DoCheck, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../models/user';
import { GLOBAL } from '../Services/global';
import { UserService } from '../Services/user.service';
import { Breadcrumb } from '../shared/page-title/breadcrumb';


@Component({
    selector: 'app-homecare',
    templateUrl: './homecare.component.html',
    providers: [UserService]
})

export class HomeCareComponent implements OnInit {
    // public numbers = new Array(10);
    public title: String;
    public url: String;
    public user: User;
    public users: User[];

    public breadcrumbs = [
        new Breadcrumb('Principal', '/'),
        new Breadcrumb('Organizaciones', '', true)
    ];

    constructor(
        private _userService: UserService,
        private _route: ActivatedRoute,
        private _router: Router,
    ) {
        this.title = 'Listado de Organizaciones';
    }

    ngOnInit() {
        this._userService.getHomeCares().subscribe(
            response => {
                if (!response.users) {

                } else {
                    this.users = response.users;
                }
            },
            error => {
                console.log(<any>error);
            }
        );
    }
}
