import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageTitleModule } from '../shared/page-title/page-title.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HomeCareComponent } from './homecare.component';
import { HttpModule } from '@angular/http';
import { UploadService } from '../Services/upload.service';
import { UserService } from '../Services/user.service';
import { User } from '../models/user';
import { GLOBAL } from '../Services/global';

@NgModule({
    declarations: [
        HomeCareComponent
    ],
    imports: [
        RouterModule, CommonModule, PageTitleModule, FormsModule, HttpModule
    ],
    providers: []
})
export class HomeCareModule { }
