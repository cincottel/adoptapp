import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageTitleModule } from '../shared/page-title/page-title.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AboutComponent } from './about.component';
import { HttpModule } from '@angular/http';
import { UploadService } from '../Services/upload.service';
import { UserService } from '../Services/user.service';
import { GLOBAL } from '../Services/global';

@NgModule({
    declarations: [
        AboutComponent
    ],
    imports: [
        RouterModule, CommonModule, PageTitleModule, FormsModule, HttpModule
    ],
    providers: []
})
export class AboutModule { }