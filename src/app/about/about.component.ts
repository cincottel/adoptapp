import { Component, OnInit } from '@angular/core';
import { Breadcrumb } from '../shared/page-title/breadcrumb';


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  title: String;
  public breadcrumbs = [
    new Breadcrumb('Principal', '/'),
    new Breadcrumb('Acerca de', '', true)
];

  constructor() {
    this.title = 'Acerca de nosotros';
   }

  ngOnInit() {
  }

}
