import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';
import { identifierName } from '@angular/compiler';
import { Animal } from '../models/animal';

@Injectable()

export class AnimalService {
    public url: string;
    public identity;
    public token;

    constructor(private _http: Http) {
        this.url = GLOBAL.url;
    }

    addAnimal(token, animal_to_register) {
        const params = JSON.stringify(animal_to_register);
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': token
        });

        return this._http.post(this.url + 'animal', params, { headers: headers })
            .map(res => res.json());
    }

    updateAnimal(token, id, animal_to_update) {
        const params = JSON.stringify(animal_to_update);
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': token
        });

        return this._http.put(this.url + 'updateAnimal/' + id, params, { headers: headers })
            .map(res => res.json());
    }

    getAnimals() {
        return this._http.get(this.url + 'animals').map(res => res.json());
    }

    getAnimal(id) {
        return this._http.get(this.url + 'animal/' + id).map(res => res.json());
    }

    getAnimalByHomeCare(homeID) {
        return this._http.get(this.url + 'animal-by-homecare/' + homeID).map(res => res.json());
    }

    deleteAnimal(token, id) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': token
        });

        const options = new RequestOptions({ headers: headers });
        return this._http.delete(this.url + 'animal/' + id, options).map(res => res.json());

    }

    getAnimalAdoptedByUser(token, UserID) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': token
        });
        return this._http.get(this.url + 'animal-by-adopter/', { headers: headers }).map(res => res.json());
    }

    adoptAnimal(token, id, AnimaltoAdopt) {
        const params = JSON.stringify(AnimaltoAdopt);

        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': token
        });
        return this._http.put(this.url + 'adoptAnimal/' + id, params, { headers: headers })
        .map(res => res.json());
    }
}
