import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';

import { NotFoundComponent } from './shared/not-found/not-found.component';

import { HomeModule } from './home/home.module';

import { AdoptionSearchModule } from './adoption-search/adoption-search.module';
import { AdoptionSearchRouting } from './adoption-search/adoption-search.routing';

import { routing } from './app.routing';
import { AdminGuard } from './Services/admin.guard';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { LoginModule } from './login/login.module';
import { LoginRouting } from './login/login.routing';

import { AdminModule } from './admin/admin.module';

import { UserEditModule } from './user-edit/user-edit.module';
import { UserEditRouting } from './user-edit/user-edit.routing';

import { AnimalDetailModule } from './animal-detail/animal-detail.module';
import { AnimalDetailRouting } from './animal-detail/animal-detail.routing';

import { HomeCareModule } from './homecares/homecare.module';
import { HomeCareRouting } from './homecares/homecare.routing';

import { MisAdopcionestModule } from './mis-adopciones/mis-adopciones.module';
import { MisAdopcionesRouting } from './mis-adopciones/mis-adopciones.routing';

import { AboutModule } from './about/about.module';
import { AboutRouting } from './about/about.routing';

@NgModule({
  declarations: [
    AppComponent, NotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    HomeModule,
    LoginRouting,
    LoginModule,
    UserEditModule,
    UserEditRouting,
    AdminModule,
    AnimalDetailModule,
    AnimalDetailRouting,
    HomeCareModule,
    HomeCareRouting,
    MisAdopcionestModule,
    MisAdopcionesRouting,
    AdoptionSearchModule,
    AdoptionSearchRouting,
    AboutModule,
    AboutRouting,
    routing
  ],
  providers: [AdminGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
