import { Component, DoCheck, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GLOBAL } from '../../../Services/global';
import { AnimalService } from '../../../Services/animal.services';
import { UserService } from '../../../Services/user.service';
import { UploadService } from '../../../Services/upload.service';
import { Animal } from '../../../models/animal';
import { User } from '../../../models/user';

@Component({
    selector: 'app-admin-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css'],
    providers: [AnimalService, UploadService, UserService]
})

export class AddComponent implements OnInit {
    public title: String;
    public url: String;
    public animal: Animal;
    public identity;
    public token;
    public status;

    public filesToUpload: Array<File>;
    fileChangeEvent(fileInput: any) {
        this.filesToUpload = <Array<File>>fileInput.target.files;
        // console.log(this.filesToUpload);
    }

    constructor(
        private _animalService: AnimalService,
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService,
        private _uploadService: UploadService
    ) {

        this.title = 'Registro Mascota';
        this.animal = new Animal('', '', '', 2017, 'NoPhoto.jpg', false, '', '', '', 'N/A', '');
        this.url = GLOBAL.url;
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();
    }

    ngOnInit() {
        console.log('add.component cargado!');
    }

    onSubmit() {
        // console.log(this.animal);
        this._animalService.addAnimal(this.token, this.animal).subscribe(

            response => {
                if (!response.animal) {
                    this.status = 'Error';
                } else {

                    this.status = 'Success';
                    this.animal = response.animal;

                    // subir la imagen del animal
                    if (!this.filesToUpload) {
                        this._router.navigate(['/admin-panel/list']);
                    } else {
                        this._uploadService.makeFileRequest(this.url + 'UploadAnimalImage/' + this.animal._id, [],
                            this.filesToUpload, this.token, 'image')
                            .then((result: any) => {
                                // console.log(this.animal);
                                this.animal.Image = result.Image;
                                this._router.navigate(['/admin-panel/list']);
                            });
                    }
                }
            },

            error => {
                const errorMessage = <any>error;

                if (errorMessage != null) {
                    this.status = 'Error';

                }
            }
        );
    }
    goBack() {
        window.history.back();
    }
}

