import { Component, DoCheck, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GLOBAL } from '../../../Services/global';
import { AnimalService } from '../../../Services/animal.services';
import { UserService } from '../../../Services/user.service';
import { UploadService } from '../../../Services/upload.service';
import { Animal } from '../../../models/animal';
import { User } from '../../../models/user';

@Component({
    selector: 'admin-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css'],
    providers: [AnimalService, UploadService, UserService]
})

export class EditComponent implements OnInit {
    public title: String;
    public url: String;
    public animal: Animal;
    public identity;
    public token;
    public status;

    public filesToUpload: Array<File>;
    fileChangeEvent(fileInput: any) {
        this.filesToUpload = <Array<File>>fileInput.target.files;
        // console.log(this.filesToUpload);
    }

    constructor(
        private _animalService: AnimalService,
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService,
        private _uploadService: UploadService
    ) {

        this.title = 'Editar Mascota';
        this.animal = new Animal('', '', '', 2017, 'NoPhoto.jpg', false, '', '', '', '', '');
        this.url = GLOBAL.url;
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();
    }

    ngOnInit() {
        console.log('edit.component cargado!');
        this.getAnimal();
    }


    getAnimal() {
        this._route.params.forEach((params: Params) => {
            const id = params['id'];

            this._animalService.getAnimal(id).subscribe(
                response => {
                    if (!response.animal) {
                        this._router.navigate(['/']);
                    } else {
                        this.animal = response.animal;
                    }

                },
                error => {
                    console.log(<any>error);
                    this._router.navigate(['/']);
                }
            );

        });
    }

    onSubmit() {
        const id = this.animal._id;
        delete this.animal.Image;
        this._animalService.updateAnimal(this.token, id, this.animal).subscribe(

            response => {
                if (!response.animal) {
                    this.status = 'Error';
                } else {

                    this.status = 'Success';
                    this.animal = response.animal;
                    this._router.navigate(['/admin-panel/list']);
                    // subir la imagen del animal
                    if (!this.filesToUpload) {
                        this._router.navigate(['/admin-panel/list']);
                    } else {
                        this._uploadService.makeFileRequest(this.url + 'UploadAnimalImage/' + this.animal._id, [],
                            this.filesToUpload, this.token, 'image')
                            .then((result: any) => {
                                // console.log(this.animal);
                                this.animal.Image = result.Image;
                                this._router.navigate(['/admin-panel/list']);
                            });
                    }
                }
            },

            error => {
                const errorMessage = <any>error;

                if (errorMessage != null) {
                    this.status = 'Error';

                }
            }
        );
    }
    goBack() {
        window.history.back();
    }
}

