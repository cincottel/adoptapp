import { Component, DoCheck, OnInit } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';

import { Animal } from '../../../models/animal';
import { User } from '../../../models/user';

import { GLOBAL } from '../../../Services/global';

import { AnimalService } from '../../../Services/animal.services';
import { UserService } from '../../../Services/user.service';

@Component({
    selector: 'admin-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css'],
    providers: [AnimalService, UserService]
})
export class ListComponent implements OnInit {
    // public numbers = new Array(10);
    public title: String;
    public url: String;
    public animal: Animal;
    public animals: Animal[];
    public identity;
    public token;
    public status;
    public busqueda;
    public adoptstatus_busqueda;

    constructor(
        private _animalService: AnimalService,
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService,

    ) {
        this.title = 'Listado de mascotas';
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();
        this.url = GLOBAL.url;

    }

    ngOnInit() {
        console.log('list.component cargado');
        this.getAnimals();
    }
    getAnimals() {
        this._animalService.getAnimalByHomeCare(this.identity._id).subscribe(
            response => {
                if (!response.animals) {

                } else {
                    this.animals = response.animals;
                }
            },
            error => {
                console.log(<any>error);
            }
        );
    }

    deleteAnimal(id) {
        this._animalService.deleteAnimal(this.token, id).subscribe(
            response => {
                if (!response.animal) {
                    alert('Error en el servidor!');
                }
                this.getAnimals();
            },
            error => {
                alert('Error en el servidor!');
            }
        );
    }
}
